package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class palidromTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testIsPalidrome() {
		assertTrue("Invalid value for palindrome", palidrom.isPalidrome("anna"));
		
	}

	@Test
	public void testIsPalidromeNegative() {
		assertFalse("Invalid value for palindrome", palidrom.isPalidrome("anna is a super car"));
		
	}
	
	@Test
	public void testIsPalidromeBoundaryIn() {
		assertTrue("Valid value for palindrome", palidrom.isPalidrome("anna"));
	
	}
	@Test
	public void testIsPalidromeBoundaryOut() {
		assertFalse("Invalid value for palindrome", palidrom.isPalidrome("annl"));
	
	}


	

}
